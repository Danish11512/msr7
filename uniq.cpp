#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
/* MSR7 - Nicolas Naing (uniq), Varun Chenna (wc), Danish Faruqi (shuf), Kartikeya
 Sharma (sort)
 Professor Skeith- CS 103
 Project 4
 */

/* I was unable to finish the uniq part of the project because I could not figure
 out how to get it working with getline() instead of cin >> n. I also could not
 figure out how the flags worked, but manually setting showcount, dupsonly, and
 uniqonly and testing from there showed that my implementation of each part was
 correct. This program also ends after one set of inputs as I couldn't figure out a
 way to detect the end of the input while using cin, which means that you have to
 press Ctrl+D to get the last line of the output and end the program.
 My program right now cannot account for multiple "conditions" (more than one of showcount, dupsonly, and uniqonly).
 
 -Nicolas
 */
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
    // define long options
    static int showcount=0, dupsonly=1, uniqonly=0; // note uniqonly is set to 1 instead of 0
    static struct option long_opts[] = {
        {"count",         no_argument, 0, 'c'},
        {"repeated",      no_argument, 0, 'd'},
        {"unique",        no_argument, 0, 'u'},
        {"help",          no_argument, 0, 'h'},
        {0,0,0,0}
    };
    // process options:
    char c;
    int opt_index = 0;
    while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
        switch (c) {
            case 'c':
                showcount = 1;
                break;
            case 'd':
                dupsonly = 1;
                break;
            case 'u':
                uniqonly = 1;
                break;
            case 'h':
                printf(usage,argv[0]);
                return 0;
            case '?':
                printf(usage,argv[0]);
                return 1;
        }
    }
    string s;
    string current;
    string prev;
    /*cout << "showcount=" << showcount << "\n";
    cout << "dupsonly=" << dupsonly << "\n";
    cout << "uniqonly=" << uniqonly << "\n";*/
    int count=0;
    int ctr=0;
    if (showcount==1) {
        while (cin >> s) {
            if (ctr==0) {
                current=s;
                count=1;
                ctr++;
            }
            else {
                prev=current;
                current=s;
                if (current!=prev) {
                    cout << count << " " << prev << "\n";
                    count=1;
                }
                else {
                    count++;
                }
            }
        }
        cout << count << " " << current << "\n";
        /*cout << "current" << " : " << current << "\n";
         cout << "prev" << " : " << prev << "\n";
         cout << "count" << " : " << count << "\n";*/
    }
    if (dupsonly==1) {
        while (cin >> s) {
            if (ctr==0) {
                current=s;
                count=1;
                ctr++;
            }
            else {
                prev=current;
                current=s;
                if (current!=prev) {
                    if (count!=1) {
                        cout << prev << "\n";}
                    count=1;
                }
                else {
                    count++;
                }
            }
        }
        if (count !=1) {
            cout << current << "\n";
        }
        /*cout << "current" << " : " << current << "\n";
         cout << "prev" << " : " << prev << "\n";
         cout << "count" << " : " << count << "\n";*/
    }
    if (uniqonly==1) {
        while (cin >> s) {
            if (ctr==0) {
                current=s;
                count=1;
                ctr++;
            }
            else {
                prev=current;
                current=s;
                if (current!=prev) {
                    if (count==1) {
                        cout << prev << "\n";}
                    count=1;
                }
                else {
                    count++;
                }
            }
        }
        if (count ==1) {
            cout << current << "\n";
        }
        /*cout << "current" << " : " << current << "\n";
         cout << "prev" << " : " << prev << "\n";
         cout << "count" << " : " << count << "\n";*/
    }
}



