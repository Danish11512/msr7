#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	//Declaring Global Variables

	string input;
	string first_string, second_string, Swap_temp;
	int first_int, second_int;
	vector <string> V, V_temp;

	if (unique == 1){

		while(getline(cin, input)){
			V.push_back(input);
		}
		/*If unique == 1, then push_back all inputs to
		vector V.*/

		for (size_t i = 0; i < V.size()-1; i++){

			for (size_t j = i+1; j < V.size(); j++){

				if (V[i] == V[j]){

					V.erase(V.begin() + j);
				}
			}
		}

		/*Run through all elements in V and compare each
		combination (nested for loop does this. Then if
		any two compared elements are the smae, erase it
		from the vector. "+j" is necessary because we
		want to remove the second time that the element
		is repeated and not the first time. This helps
		maintain order.*/
	}

	else{

		while(getline(cin, input)){
			V.push_back(input);
		}
	}
	/*If unique != 1, then just take in the inputs from
	the file and store it in vector V.*/

	for (size_t i = 0; i < V.size() - 1; i++){

		for (size_t j = i+1; j < V.size(); j++ ){

			first_string = V[i];
			second_string = V[j];

			/*To sort the inputs in a case-insensitive way,
			we can use use the principles of the ascii test
			and again, run through the vector and take in
			the first word and the second word as
			"first_string" and "second_string".*/

			for(size_t k = 0; k < first_string.length(); k++){

				first_int = first_string[k];
				second_int = second_string[k];

				/*Convert the first and seocnd strings to integers
				so they can be compared and changed if lowercase.*/

				if (ignorecase == 1){

					if(first_int > 96 && first_int < 123){

						first_int -= 32;
					}

					if(second_int > 96 && second_int < 123){

						second_int -= 32;
					}
				}
				/*If the first or second string is lowercase,
				change it to uppercase, hence the -32.*/

				if(first_int > second_int){

					Swap_temp = V[i];
					V[i] = V[j];
					V[j] = Swap_temp;
					break;
				}
				/*If the first string was alphabetically larger
				than the second string, then switch them. I have used
				a temp string to swap both strings.*/

				else if(first_int < second_int){

					break;
				}
				/*If it is already alphabetized, then do nothing. */
			}
		}
	}

	if (descending == 1){

		V_temp = V;

		for(size_t i = V.size() - 1; V.size() > i; i--){
			cout << V_temp[i] << "\n";
		}
	}
	/*Since we already have stored the inputs in vector V,
	We can just reverse it through a for loop. By storing
	the original vector V in V_temp, we can reverse the
	vector.*/

	else{

		for(size_t i = 0; i < V.size(); i++){
			cout << V[i] << "\n";
		}
	}
	/*This else statement prints out each string of V in
	a new line. This includes -u and -f.*/

	return 0;
}
